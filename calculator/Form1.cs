﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public partial class Form1 : Form
    {
        int historyIter = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void plusButton_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "+";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
                
        }

        private void minusButton_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "-";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
        }

        private void proizvButton_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "*";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                operationLabel.Visible = true;
                operationLabel.Text = "/";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
        }

        private void equalButton_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            equalLabel.Visible = false;

            if (textBox1.Text.Length != 0 && textBox2.Text.Length != 0)
            {
                equalLabel.Visible = true;
                equalLabel.Text = "=";
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите второй аргумент!";
            }

            double x, y, z = 0.0;

            try
            {
                x = double.Parse(textBox1.Text);
                y = double.Parse(textBox2.Text);
                z = 0.0;
                
                switch(operationLabel.Text)
                {
                    case "+":
                        z = x + y;
                        break;
                    case "-":
                        z = x - y;
                        break;
                    case "*":
                        z = x * y;
                        break;
                    case "/":
                        z = x / y;
                        if (y == 0)
                        {
                            errorLabel.Text = "Ошибка! Деление на ноль.";
                            errorLabel.Visible = true;
                        }
                        break;
                    case "% от":
                        z = y * x / 100;
                        break;
                }
                z = Math.Round(z, 5);
                if (historyIter == 5)
                {
                    historyLabel.Text = "История операций:";
                    historyIter = 0;
                }
                historyLabel.Text += ("\n" + textBox1.Text + operationLabel.Text 
                    + textBox2.Text + " = " + z);
                historyIter += 1;
            }
            catch(Exception ex)
            {
                errorLabel.Text = "Ошибка! Неверно заданы аргументы.\nПопробуйте использовать точку в качестве разделителя.";
                errorLabel.Visible = true;
            }

            textBox3.Text = z.ToString();
        }

        private void cBitton_Click(object sender, EventArgs e)
        {
            operationLabel.Visible = false;
            operationLabel.Text = "null";
            equalLabel.Visible = false;
            equalLabel.Text = "null";
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            errorLabel.Visible = false;
            errorLabel.Text = "null";
        }

        private void persentButton_Click(object sender, EventArgs e)
        {
            errorLabel.Visible = false;
            operationLabel.Visible = false;

            if (textBox1.Text.Length != 0)
            {
                if (double.Parse(textBox1.Text) > 0)
                {
                    operationLabel.Visible = true;
                    operationLabel.Text = "% от";
                }
                else
                {
                    errorLabel.Visible = true;
                    errorLabel.Text = "Значение процента должно быть больше нуля!";
                }
            }
            else
            {
                errorLabel.Visible = true;
                errorLabel.Text = "Введите первый аргумент!";
            }
        }
    }
}
