﻿namespace calculator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.plusButton = new System.Windows.Forms.Button();
            this.minusButton = new System.Windows.Forms.Button();
            this.proizvButton = new System.Windows.Forms.Button();
            this.delButton = new System.Windows.Forms.Button();
            this.equalButton = new System.Windows.Forms.Button();
            this.cButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.operationLabel = new System.Windows.Forms.Label();
            this.equalLabel = new System.Windows.Forms.Label();
            this.errorLabel = new System.Windows.Forms.Label();
            this.persentButton = new System.Windows.Forms.Button();
            this.historyLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // plusButton
            // 
            this.plusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.plusButton.Location = new System.Drawing.Point(12, 113);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(40, 40);
            this.plusButton.TabIndex = 0;
            this.plusButton.Text = "+";
            this.plusButton.UseVisualStyleBackColor = true;
            this.plusButton.Click += new System.EventHandler(this.plusButton_Click);
            // 
            // minusButton
            // 
            this.minusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minusButton.Location = new System.Drawing.Point(70, 113);
            this.minusButton.Name = "minusButton";
            this.minusButton.Size = new System.Drawing.Size(40, 40);
            this.minusButton.TabIndex = 1;
            this.minusButton.Text = "-";
            this.minusButton.UseVisualStyleBackColor = true;
            this.minusButton.Click += new System.EventHandler(this.minusButton_Click);
            // 
            // proizvButton
            // 
            this.proizvButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.proizvButton.Location = new System.Drawing.Point(127, 113);
            this.proizvButton.Name = "proizvButton";
            this.proizvButton.Size = new System.Drawing.Size(40, 40);
            this.proizvButton.TabIndex = 2;
            this.proizvButton.Text = "*";
            this.proizvButton.UseVisualStyleBackColor = true;
            this.proizvButton.Click += new System.EventHandler(this.proizvButton_Click);
            // 
            // delButton
            // 
            this.delButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.delButton.Location = new System.Drawing.Point(186, 113);
            this.delButton.Name = "delButton";
            this.delButton.Size = new System.Drawing.Size(40, 40);
            this.delButton.TabIndex = 3;
            this.delButton.Text = "/";
            this.delButton.UseVisualStyleBackColor = true;
            this.delButton.Click += new System.EventHandler(this.delButton_Click);
            // 
            // equalButton
            // 
            this.equalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.equalButton.Location = new System.Drawing.Point(299, 113);
            this.equalButton.Name = "equalButton";
            this.equalButton.Size = new System.Drawing.Size(40, 40);
            this.equalButton.TabIndex = 4;
            this.equalButton.Text = "=";
            this.equalButton.UseVisualStyleBackColor = true;
            this.equalButton.Click += new System.EventHandler(this.equalButton_Click);
            // 
            // cButton
            // 
            this.cButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cButton.Location = new System.Drawing.Point(357, 113);
            this.cButton.Name = "cButton";
            this.cButton.Size = new System.Drawing.Size(40, 40);
            this.cButton.TabIndex = 5;
            this.cButton.Text = "С";
            this.cButton.UseVisualStyleBackColor = true;
            this.cButton.Click += new System.EventHandler(this.cBitton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 46);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(98, 22);
            this.textBox1.TabIndex = 6;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(159, 46);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(98, 22);
            this.textBox2.TabIndex = 7;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(299, 46);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(98, 22);
            this.textBox3.TabIndex = 8;
            // 
            // operationLabel
            // 
            this.operationLabel.AutoSize = true;
            this.operationLabel.Location = new System.Drawing.Point(116, 49);
            this.operationLabel.Name = "operationLabel";
            this.operationLabel.Size = new System.Drawing.Size(27, 16);
            this.operationLabel.TabIndex = 9;
            this.operationLabel.Text = "null";
            this.operationLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.operationLabel.Visible = false;
            // 
            // equalLabel
            // 
            this.equalLabel.AutoSize = true;
            this.equalLabel.Location = new System.Drawing.Point(266, 49);
            this.equalLabel.Name = "equalLabel";
            this.equalLabel.Size = new System.Drawing.Size(27, 16);
            this.equalLabel.TabIndex = 10;
            this.equalLabel.Text = "null";
            this.equalLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.equalLabel.Visible = false;
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Location = new System.Drawing.Point(13, 71);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(27, 16);
            this.errorLabel.TabIndex = 11;
            this.errorLabel.Text = "null";
            this.errorLabel.Visible = false;
            // 
            // persentButton
            // 
            this.persentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F);
            this.persentButton.Location = new System.Drawing.Point(244, 113);
            this.persentButton.Name = "persentButton";
            this.persentButton.Size = new System.Drawing.Size(40, 40);
            this.persentButton.TabIndex = 12;
            this.persentButton.Text = "%";
            this.persentButton.UseVisualStyleBackColor = true;
            this.persentButton.Click += new System.EventHandler(this.persentButton_Click);
            // 
            // historyLabel
            // 
            this.historyLabel.AutoSize = true;
            this.historyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.historyLabel.Location = new System.Drawing.Point(12, 177);
            this.historyLabel.Name = "historyLabel";
            this.historyLabel.Size = new System.Drawing.Size(171, 22);
            this.historyLabel.TabIndex = 13;
            this.historyLabel.Text = "История операций:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 326);
            this.Controls.Add(this.historyLabel);
            this.Controls.Add(this.persentButton);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.equalLabel);
            this.Controls.Add(this.operationLabel);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cButton);
            this.Controls.Add(this.equalButton);
            this.Controls.Add(this.delButton);
            this.Controls.Add(this.proizvButton);
            this.Controls.Add(this.minusButton);
            this.Controls.Add(this.plusButton);
            this.Name = "Form1";
            this.Text = "Калькулятор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.Button minusButton;
        private System.Windows.Forms.Button proizvButton;
        private System.Windows.Forms.Button delButton;
        private System.Windows.Forms.Button equalButton;
        private System.Windows.Forms.Button cButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label operationLabel;
        private System.Windows.Forms.Label equalLabel;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.Button persentButton;
        private System.Windows.Forms.Label historyLabel;
    }
}

